# A simple RESTful service that allows to manage user’s todo’s.

## Requirements:
1. Typescript
2. Framework of choice
3. Authentication is based on HTTP header with user ID, no need to implement users.
4. REST endpoints to add, update, delete, list user’s todo’s.
5. Tests
6. Documentation
7. Dockerfile
8. Everything unclear is for developer decision

# Quick start / Production
```bash
docker-compose up
```

The service is available at http://localhost:12345

NOTE: Is important to shutdown any running docker containers with dev/test postgres instances using default port 5432. 

# Documentation

API documentation is available at http://localhost:12345/api-docs once the service started.


# Development: Running the app
1. Install node modules (first time only)
```bash
npm ci
```
2. Run development database
```bash
docker run -it -e "POSTGRES_USER=postgres" -e "POSTGRES_PASSWORD=postgres" -e "POSTGRES_DB=todo_dev" -p 5432:5432 postgres
```
3. Run the service:
```bash
npm run dev
```

# Testing
1. Run test database:
```bash
docker run -it -e "POSTGRES_USER=postgres" -e "POSTGRES_PASSWORD=postgres" -e "POSTGRES_DB=todo_test" -p 5432:5432 postgres
```

1. Run tests
```bash
npm run test
```
or some subset
```bash
npm run test database
npm run test app
```

# Command line examples
## Get list of tasks
```bash
curl -H "Authorization: USER_ID 123" localhost:12345/tasks
```

## Create a task
```bash
curl -X POST localhost:12345/tasks \
   -H "Authorization: USER_ID 123" \
   -H 'Content-Type: application/json' \
   -d '{"title": "deploy the app", "description": "do something"}'
```

## Retrive a task
```bash
curl -H "Authorization: USER_ID 123" localhost:12345/tasks/1
```

## Update a task
```bash
curl -X PUT localhost:12345/tasks/1 \
   -H "Authorization: USER_ID 123" \
   -H 'Content-Type: application/json' \
   -d '{"title":"deploy the app","description":"install new server, deploy the app"}'
```

## Delete a task
```bash
curl -X DELETE -H "Authorization: USER_ID 123" localhost:12345/tasks/1
```
