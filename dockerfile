FROM node:17.7.2-alpine as builder

COPY package*.json /tmp/

RUN cd /tmp && npm install
RUN mkdir -p /opt/app && cp -a /tmp/node_modules /opt/app

WORKDIR /opt/app

COPY . /opt/app/
RUN npm run build

FROM node:17.7.2-alpine

WORKDIR /opt/app
COPY package*.json ./

# We run as root for simplicity.
# USER node

ENV NODE_ENV production
RUN npm install --production
COPY --from=builder /opt/app/build ./build

COPY /config ./config
# a little fix for swagger
COPY /src/swagger.json ./build/src/swagger.json

EXPOSE 12345
CMD [ "node", "/opt/app/build/main.js" ]
