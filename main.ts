import { app } from "./src/app";
import config from "config";
import connection from "./src/connection";

const port = config.get<number>("port");

app.listen(port, async () => {
    console.log(`Todo Service is listening on port ${port}.`);
    console.log(`API documentation is available at http://localhost:${port}/api-docs`)
    try {
        await connection.sync();
        console.log("Connected to DB.");
    } catch (error) {
        console.error(`Could not connect to db: ${error}.`);
        process.exit(1);
    }
});
