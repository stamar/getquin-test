
import supertest from "supertest";
import { app } from "./app";
import connection from "./connection";

describe("App", () => {

    beforeAll(async() => {
        // Drop and recreate tables in the test database.
       await connection.sync({force: true}); 
    });

    const credentials1 = { Authorization: "USER_ID 123" };
    const credentials2 = { Authorization: "USER_ID 234" };


    it("should answer 401 without authentication", async () => {
        await supertest(app).get("/tasks").expect(401);
    });

    it("should return empty list of tasks", async () => {
        const response = await supertest(app).get("/tasks").set(credentials1).expect(200);
        expect(response.body).toEqual([]);
    });

    it("should return 404 if task doesn't exist", async () => {
        await supertest(app).get("/tasks/0987654321").set(credentials1).expect(404);
    });

    it("should create task for user1", async () => {
        const task = {
            title: "urgent action",
            description: "this should be done as soon as possible",
            dueDate: new Date("2022-04-01"),
            done: false
        };
        await supertest(app).post("/tasks").set(credentials1).send(task).expect(200);
    });

    let taskId:any = null;
    it("should return list with one tasks for user1", async () => {
        const response = await supertest(app).get("/tasks").set(credentials1).expect(200);
        expect(response.body.length).toEqual(1);
        taskId = response.body[0].id;
        const task = response.body[0];
        expect(task.title).toBe("urgent action");
        expect(task.description).toBe("this should be done as soon as possible");
        expect(new Date(task.dueDate)).toEqual(new Date("2022-04-01"))
        expect(task.done).toBeFalsy();
    });

    it("should return empty list of tasks for user2", async () => {
        const response = await supertest(app).get("/tasks").set(credentials2).expect(200);
        expect(response.body).toEqual([]);
    });

    it("should update task for user1", async () => {
        const task = {
            title: "new title",
            description: "new description",
            dueDate: new Date("2022-05-01"),
            done: true
        };
        const response = await supertest(app).put(`/tasks/${taskId}`).set(credentials1).send(task).expect(200);
        const updatedTask = response.body;
        expect(updatedTask.title).toBe("new title");
        expect(updatedTask.description).toBe("new description");
        expect(new Date(updatedTask.dueDate)).toEqual(new Date("2022-05-01"))
        expect(updatedTask.done).toBeTruthy();
    });

    it("should delete task for user1", async () => {
        await supertest(app).delete(`/tasks/${taskId}`).set(credentials1).expect(200);
    });

    it("should return 404 on attempt to delete nonexistent task for user1", async () => {
        await supertest(app).delete(`/tasks/${taskId}`).set(credentials1).expect(404);
    });

});
