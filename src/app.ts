import express, {Request, Response} from "express";
import config from "config";
import swaggerUi from "swagger-ui-express";
const swaggerDocument = require("./swagger.json");

import auth from "./middleware/auth"
import { router } from "./routes/index";

const port = config.get<number>("port");

export const app: express.Application = express();

app.use(express.json());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use('/tasks', auth, router);

// Brief service description.
app.use("/", (req: Request, res: Response) => {
    res.send('A simple RESTful service that allows to manage user’s todo’s. <a href="/api-docs">API documentaion.</a>');
});
