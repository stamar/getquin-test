import { Sequelize } from "sequelize-typescript";
import config from "config";

const dbConfig = config.get<Object>("db");

const connection = new Sequelize({
  ...dbConfig,
  logging: false,
  models: [__dirname + '/models']
});

export default connection;
