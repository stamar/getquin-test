import connection from "./connection";
import Task from "./models/task"

beforeAll(async () => {
    await connection.sync({force: true});
});

test('create task', async() => {
    const task = await Task.create({
        userId: 1001,
        title: "Test task",
        description: "just do it"
    });
    expect(task.id).toEqual(1);
});
