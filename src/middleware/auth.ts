import {Request, Response, NextFunction} from "express";

/**
 * This is authentication middleware based on given user id only.
 * It simply get user id from Authorization header with custom scheme USER_ID.
 * It produces 401 if user id is not given in Authorization header.
 */
export default function auth(req:Request, res:Response, next:NextFunction) {

    const authHeader = req.header('authorization');
    if (authHeader && authHeader.match(/USER_ID .*/)) {
        const userId = authHeader.replace(/USER_ID /, "");
        res.locals.userId = userId;
        return next();
    } else {
        return res.status(401).send("No user id provided.");
    }
};
