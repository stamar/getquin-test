import { Table, Model, Column, DataType } from "sequelize-typescript";

@Table({
  tableName: "tasks",
})
export default class Task extends Model {

    // As we don't have Users table we just imitate userId is foreign key.
    // @ForeignKey(() => User)
    @Column({
        type: DataType.INTEGER,
        allowNull: false
    })
    userId!: number;
  
    // @BelongsTo(() => User)
    // user: User

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  title!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  description!: string;

  @Column({
    type: DataType.DATE,
    allowNull: true,
  })
  dueDate!: Date;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  })
  done!: boolean;
};
