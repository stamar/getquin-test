import express, { Request, Response } from "express";
import Task from "../models/task"

export const router = express.Router();

router.get("/", async (req: Request, res: Response) => {
    const userId = res.locals.userId;
    const tasks = await Task.findAll({
        where: {
            userId: userId
        }
    });
    res.status(200).send(tasks);
})

router.get("/:taskId", async (req: Request, res: Response) => {
    const userId = res.locals.userId;
    const taskId = req.params.taskId;
    const task = await Task.findOne({
        where: {
            id: taskId,
            userId
        }
    });
    if (task) {
        res.status(200).send(task);
    } else {
        return res.status(404).send(`Task with id=${taskId} is not found.`);
    }
    
});


router.post("/", async (req: Request, res: Response) => {
    const { title, description, dueDate, } = req.body;
    const userId = res.locals.userId;
    try {
        const task = await Task.create({title, description, dueDate, userId});
        return res.status(200).send(task);
    } catch (error: any) {
        return res.status(400).send(error.message);
    }
});


router.put("/:taskId", async (req: Request, res: Response) => {
    const userId = res.locals.userId;
    const taskId = req.params.taskId;
    const updatedTask = req.body;
    const task = await Task.findOne({
        where: {
            id: taskId,
            userId
        }
    });
    if (task) {
        try {
            task.update(updatedTask);
            return res.status(200).send(task);
        } catch (error: any) {
            return res.status(400).send(error.message);
        }
        
    } else {
        return res.status(404).send(`Task with id=${req.params.taskId} is not found.`);
    }
});

router.delete("/:taskId", async (req: Request, res: Response) => {
    const userId = res.locals.userId;
    const taskId = req.params.taskId;
    const rowDeleted = await Task.destroy({
        where: {
            id: taskId,
            userId
        }
    });
    if (!rowDeleted) {
        return res.status(404).send(`Task with id=${taskId} is not found.`);
    }
    return res.status(200).end();
});
  